const mongoose = require("mongoose");


const product_Schema = new mongoose.Schema({
	name:{
		type: String,
		required:[true, "Name is required"]
	},
	description:{
		type: String,
		required:[true, "Description is required"]
	},
	price: {
		type: Number,
		required:[true, "Price is required"]
	},
	isActive:{
		type: Boolean,
		default: true
	},
	createdOn:{
		type: Date,
		default: new Date()
	},
	orders:[
		{
			orderId:{
				type:String,
				default:String
			},
			userId:{
				type:String,
				required:[true, "User Id is required"]
			},
			email:{
				type:String,
				required:['User email is required']
			}
		}
	]
})
module.exports = mongoose.model("Product", product_Schema);
