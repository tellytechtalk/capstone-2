const mongoose = require('mongoose')

const user_schema = new mongoose.Schema({
		firstName: {
			type: String,
			required: [true, "First Name is required."]
		}, 
		lastName: {
			type: String,
			required: [true, "Last Name is required."]
		}, 
		email: {
			type: String,
			required: [true, "Email is required."]
		},
		password: {
			type: String,
			required: [true, "Password is required."]
		},
		isAdmin: {
			type: Boolean, 
			default: false
		}, 
		mobileNo: {
			type: String,
			required: [true, 'Mobile Number is required']
		},
		orders: [
		{
			productId:{
				type: String,
				required: [true, 'Product Id required']
			}, 
			price: {
				type: String,
				required: [true, 'Price is required']
			},

			productName: {
				type: String, 
				required: [true, 'Price is required']
			}, 

			quantity: {
				type: Number,
				required: [true, 'Quantity is required']
			}, 

			purchasedOn: {
			type: Date,
			default: new Date()
		},
			TotalAmount : {
			type: Number,
			required: [true, 'Total amount is required']

		}
	}]
})


module.exports = mongoose.model('User', user_schema)