const express = require('express')
const router = express.Router()
const UserController = require('../controllers/UserController')
const auth = require('../auth')

// CHECK IF EMAIL EXISTS

router.post('/check-email', (request, response) => {

	try {

		UserController.checkIfEmailExists(request.body).then((result) =>
	{
			response.send(result)

	})

		} catch(error) {
			return error.message
	}
})


// USER REGISTRATION

router.post('/register', (request,response) => {

	try {

		UserController.register(request.body).then((result)=> {
			response.send(result)
	})

		} catch(error) {  
			return error.message
	}
})


//LOGIN USER AUTHENTICATION 

router.post('/login',(request,response) => {

	try {

		UserController.login(request.body).then((result) => {
			response.send(result)
	})

		} catch(error){ 
			return error.message
	}
})





//NON-ADMIN ORDER PRODUCT

router.post('/order', auth.verify,(request,response) =>{

		try {

			let userId = auth.decode(request.headers.authorization).id;
			let email = auth.decode(request.headers.authorization).email;
			let isAdmin = auth.decode(request.headers.authorization).isAdmin; 
			let data = {
				userId: userId,
				productId: request.body.productId,
				quantity: request.body.quantity,
				email: email
			};

			if (!isAdmin) {
				UserController.createOrder(data).then(result =>{
				
					response.send(result);

				}) 

			} else {
				response.send('Login with your personal account')
			}

		} catch (error) {
			return error.message
	}
			
})





//RETRIEVE SINGLE USER DETAILS 

router.get("/:id/details", auth.verify, (request, response) => {
 
	try {
		
		UserController.getUserDetails(request.params.id).then((result) => {
		response.send(result)

		})

		} catch(error) {
			return error.message
	}
})





//USER SET AS ADMIN 

router.patch('/:userId/userUpdate', auth.verify,(request,response) => {

	try {

		let isAdmin = auth.decode(request.headers.authorization).isAdmin
		
		isAdmin?UserController.userAsAdmin(request.params.userId).then(result =>
		 response.send(result)): response.send('ACTION FORBIDDEN')

		} catch(error) {
			return error.message
	}
})


module.exports = router