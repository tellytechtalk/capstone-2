const express = require('express')
const router = express.Router()
const ProductController = require('../controllers/ProductController')
const auth = require('../auth')


// CREATE SINGLE PRODUCT

router.post('/create', auth.verify, (request, response) => {
	const data = {
		product: request.body,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

    ProductController.addProduct(data).then((result) => {
        response.send(result)
   	 })
  })



//RETRIEVE ALL PRODUCTS 

router.get('/', (request, response) => {
	ProductController.getAllProducts().then((result) => {
		response.send(result)
	})
})



// RETRIEVE ALL ACTIVE PRODUCTS

router.get('/active', (request, response) => {
	ProductController.getAllActive().then((result) => {
		response.send(result)
	})
})



// RETRIEVE A SINGLE PRODUCT

router.get('/:productId', (request, response) => {
	ProductController.getProduct(request.params.productId).then((result) => {
		response.send(result)
	})
})




// UPDATE A PRODUCT INFORMATION (ADMIN ONLY)

router.patch('/:productId/update', auth.verify,(request,response) =>{

		let isAdmin = auth.decode(request.headers.authorization).isAdmin
		isAdmin ? ProductController.updateProduct(request.params, request.body).then(result => {
			
		response.send(result)}):response.send('You must be an Admin to access this!')
	})




// ARCHIVE A SINGLE PRODUCT 

router.patch('/:productId/archive', auth.verify, (request, response) => {
	let isAdmin = auth.decode(request.headers.authorization).isAdmin
		isAdmin ? ProductController.archiveProduct(request.params, request.body).then(result => {
			
		response.send(result)}):response.send('You must be an Admin to access this!')
	})





module.exports = router
