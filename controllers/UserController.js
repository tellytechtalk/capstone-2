const User = require('../models/User')
const Product = require('../models/Product')
const Order = require('../models/Product');
const bcrypt = require('bcrypt')
const auth = require('../auth')


//CHECK EMAIL 
module.exports.checkIfEmailExists = (data) => {

	try{
		return User.find({email: data.email}).then((result) =>{
		if(result.length > 0){
			return 'This email exists!'
		}

		return 'This email does not exists!'
	})

		} catch(error) {

			return error.message
		}
}


//USER REGISTRATION 

module.exports.register = (data) => {

try { 

	let encrypted_password = bcrypt.hashSync(data.password,10)

	let new_user = new User({
		firstName: data.firstName,
		lastName: data.lastName,
		email: data.email,
		mobileNo: data.mobileNo,
		password: encrypted_password
	})

	return new_user.save().then((created_user, error) => {
		if(error){
			return 'Something went wrong.'
		}
			return  'User successfully registered!'
	})

	} catch(error){  

		return error.message
	}
}




//LOGIN USER AUTHENTICATION 

module.exports.login = (data) => {

	try {

	return User.findOne({email: data.email}).then((result) => {
		if(result == null){
			return 'User does not exist!'
		}

		const is_password_correct = bcrypt.compareSync(data.password, result.password)

		if(is_password_correct) {
			return {
				accessToken: auth.createAccessToken(result)
			}
		}

		return 'Password is incorrect!'
	})

	} catch(error){
		return error.message 
	}
}



//SET USER  AS ADMIN

module.exports.userAsAdmin = (userId) => {


	try{
		let updatedUser = {
			isAdmin : true
		}
		return User.findByIdAndUpdate(userId, updatedUser).then((result, error) => {
			if (result) {
				return `Successfully update user role as Admin`
			} else {
				return `Something went wrong.`
			}
		})

		} catch(error){
			return error.message}
	}




//NON-ADMIN CHECKOUT 

module.exports.createOrder = async (data) => {
	
		try {
			let id = data.userId;
			let product = data.productId;
			let quantity = data.quantity
		   
				let total = await Product.findById(product).then(product => {
					let productId = product.id
					let productName = product.name
					let productPrice = product.price
				
	  
				let userCreateOrder = User.findById(id).then(user => {
					let TotalAmount = user.orders.reduce((total) => {
						
							return total + (product.price * quantity)
					},0)
						
					let newData = {
						  productId : productId,
						  price : productPrice,
						  productName: productName , 
						  quantity : quantity, 
						 
					}
					console.log(newData)

					user.orders.push (
						
					{
						TotalAmount:TotalAmount, 
						productId :newData.productId,
						price: newData.price, 
						productName:newData.productName, 
						quantity: newData.quantity

					}
				)
					return user.save().then((saved, error) => {
						if (saved) {
							return saved
						} else {
							return false
						}
					})
				})
				return true
		   })
		  let isProductUpdated = await Product.findById(product).then(products => {
					products.orders.push(
						{ 
							userId: id, 
							email : data.email
						}
					)
				 return products.save().then((saved, error) => {
					if (error) {
					   return false;
					} else {
				  
					   return saved; 
					} 
				 })
		   })
		 
		  return total && isProductUpdated
		} catch (error) {
			
			return error.message
	}
}



//RETRIEVE SINGLE USER DETAILS

try{

module.exports.getUserDetails = (user_id) => {
	return User.findById(user_id).then((result) => {
		return result 
	})

	} catch(error) {
		return error.message }
}



