const Product  = require('../models/Product')
const User = require('../models/User')


//CREATE PRODUCT

module.exports.addProduct = (data) => {
		if(data.isAdmin){

		let new_product = new Product({
			name: data.product.name,
			description: data.product.description, 
			price: data.product.price
		})


		return new_product.save().then((new_product, error) => {
			if(error){
				return false
			}

			return 'New product successfully created!'

		})

	}
	
	let message = Promise.resolve({
		message:'User must be ADMIN to access this.'
	})
	return message.then((value) => {
		return value
	})
}


//RETRIEVE ALL PRODUCTS 

module.exports.getAllProducts = () => {
	return Product.find({}).then((result) => {
		return result
	})
}


//RETRIEVE ALL ACTIVE PRODUCTS 

module.exports.getAllActive = () => {
	return Product.find({isActive: true}).then((result) => {
		return result
	})
}


//RETRIEVE A SINGLE PRODUCT

module.exports.getProduct = (product_id) => {
	return Product.findById(product_id).then((result) => {
		return result 
	})
}



//UPDATE A PRODUCT INFORMATION (ADMIN ONLY)
	module.exports.updateProduct = (product, product_update) => {

		let updatedProduct = {

			name : product_update.name,
			description : product_update.description,
			price : product_update.price,
		}

		let id = product.productId
		return Product.findByIdAndUpdate(id, updatedProduct).then((result, error) => {
				if (result) {
					return 'Product has been updated successfully!'
				} else {
					return `error updating the product`
				}
		})
	}


//ARCHIVE PRODUCT (ADMIN ONLY)

module.exports.archiveProducts = (product, archived_product) => {
		let new_status = {
			isActive : false
		}
	
			let id = product.productId
		return Product.findByIdAndUpdate(id, new_status).then((result, error) =>{
			if (result) {
				return 'Product has been archived successfully!'
			} else {
				return `error updating the product`
			}
		})
	}



